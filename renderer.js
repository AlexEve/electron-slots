// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// All of the Node.js APIs are available in this process.

console.log("welcome to my world");

class Spinner {
    constructor() {
        this.spinner1 = document.querySelector('.spinner-1');
        this.spinner2 = document.querySelector('.spinner-2');
        this.spinner3 = document.querySelector('.spinner-3');
        this.spinner4 = document.querySelector('.spinner-4');
        this.spinner5 = document.querySelector('.spinner-5');
        this.spinner6 = document.querySelector('.spinner-6');
        this.spinner7 = document.querySelector('.spinner-7');
        this.spinner8 = document.querySelector('.spinner-8');
        this.spinner9 = document.querySelector('.spinner-9');

        this.spinnersList = [
            document.querySelector('.spinner-1'),
            document.querySelector('.spinner-2'),
            document.querySelector('.spinner-3'),
            document.querySelector('.spinner-4'),
            document.querySelector('.spinner-5'),
            document.querySelector('.spinner-6'),
            document.querySelector('.spinner-7'),
            document.querySelector('.spinner-8'),
            document.querySelector('.spinner-9')
        ];

        this.spinButton = document.querySelector('.spin-start');

        this.results = document.querySelector('.results');

        this.cash = 10;
        this.cashDisplay = document.querySelector('.cash');

        this.stake = document.querySelector('.stake');

        this.spinning = false;

        this.spinLength = 4; //Seconds

        this.init();
    }

    init() {
        this.addHandlers();
        this.setupReels();
        this.cashDisplay.innerHTML = this.cash;

        Notification.requestPermission().then((result) => {
            console.log(result);
        });
    }

    addHandlers() {
        this.spinButton.addEventListener('click', () => {
            this.startSpinner();    
        });
    }

    startSpinner() {
        console.log("Spinner start");

        /*var myNotification = new Notification('Spinner', {
            body: 'Spin spin to win!'
        });*/

        // Ignore clicks if already spinning
        if (this.spinning) {
            return;
        }

        if(this.cash <= 0) {
            alert("You're out of money, loser!");
            return;
        }

        var stake = this.stake.value;

        if(stake > this.cash) {
            alert("Please lower your stake and try again");
            return;
        }

        this.updateCash(-stake);

        this.spinning = true;
        this.results.innerHTML = 'Spinning...';
        this.results.style.color = '#000000';

        var timeout = 10;
        var spinTime = this.spinLength * 1000; // Convert to ms
        var startTime = Date.now();

        this.spinReels(startTime, timeout, spinTime, stake);
    }

    updateCash(cashChange) {
        this.cash = this.cash + cashChange;
        this.cashDisplay.innerHTML = this.cash;
    }

    spinReels(startTime, timeout, spinTime, stake) {
        this.updateReels();

        if ((Date.now() - startTime) >= spinTime) {
            this.finishSpin(stake);
        } else {
            // Slow down a bit on each spin
            timeout *= 1.1;
            setTimeout(() => {
                this.spinReels(startTime, timeout, spinTime, stake);
            }, timeout);
        }
    }

    setupReels() {
        for(var r = 0; r < this.spinnersList.length; r++) {
            this.spinnersList[r].innerHTML = this.getRandomValue();
        }
    }

    updateReels() {
        this.spinnersList[6].innerHTML = this.spinnersList[3].innerHTML;
        this.spinnersList[7].innerHTML = this.spinnersList[4].innerHTML;
        this.spinnersList[8].innerHTML = this.spinnersList[5].innerHTML;

        this.spinnersList[3].innerHTML = this.spinnersList[0].innerHTML;
        this.spinnersList[4].innerHTML = this.spinnersList[1].innerHTML;
        this.spinnersList[5].innerHTML = this.spinnersList[2].innerHTML;

        this.spinnersList[0].innerHTML = this.getRandomValue();
        this.spinnersList[1].innerHTML = this.getRandomValue();
        this.spinnersList[2].innerHTML = this.getRandomValue();
    }

    getRandomValue() { // Weighted random the old fashioned way
        var values = [
            '7️⃣',
            '💎',
            '⭐', '⭐',
            '🍀', '🍀',
            '🔔', '🔔',
            '🍒', '🍒',
            '🍊', '🍊',
            '🍇', '🍇',
        ];
        return values[Math.floor(Math.random() * values.length)];
    }

    getStakeReturn(stake, rowModifier, icon) {
        console.log("%s, %s, %s", stake, rowModifier, icon);
        var iconValue = 1;
        switch(icon) {
            case '7️⃣': // Seven
                iconValue = 2.5;
                break;
            case '💎': // Diamond
                iconValue = 2;
                break;
            case '⭐': // Star
                iconValue = 1.75;
                break;
            case '🍀': // Clover
                iconValue = 1.5;
                break;
            case '🔔': // Bell
                iconValue = 1.5;
                break;
            case '🍒': // Cherry
                iconValue = 1;
                break;
            case '🍇': // Grape
                iconValue = 1;
                break;
            case '🍊': // Orange
                iconValue = 1;
                break;
            default:
                iconValue = 1;
                break;
        }

        //console.log(iconValue);
        //console.log(stake * iconValue * rowModifier);
        
        return Math.round(stake * iconValue * rowModifier);
    }

    finishSpin(stake) {
        this.spinning = false;

        console.log(stake);

        var topLine = new Set([this.spinnersList[0].innerHTML, this.spinnersList[1].innerHTML, this.spinnersList[2].innerHTML]);
        var midLine = new Set([this.spinnersList[3].innerHTML, this.spinnersList[4].innerHTML, this.spinnersList[5].innerHTML]);
        var botLine = new Set([this.spinnersList[6].innerHTML, this.spinnersList[7].innerHTML, this.spinnersList[8].innerHTML]);

        var diag1Line = new Set([this.spinnersList[0].innerHTML, this.spinnersList[4].innerHTML, this.spinnersList[8].innerHTML]);
        var diag2Line = new Set([this.spinnersList[2].innerHTML, this.spinnersList[4].innerHTML, this.spinnersList[6].innerHTML]);

        if(midLine.size == 1) {
            var stakeReturn = this.getStakeReturn(stake, 2, this.spinnersList[4].innerHTML);
            this.results.innerHTML = '💰 Jackpot! +💲'+ stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(topLine.size == 1) {
            var stakeReturn = this.getStakeReturn(stake, 1.5, this.spinnersList[1].innerHTML);
            this.results.innerHTML = '💰 Jackpot! +💲'+ stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(botLine.size == 1) {
            var stakeReturn = this.getStakeReturn(stake, 1.5, this.spinnersList[7].innerHTML);
            this.results.innerHTML = '💰 Jackpot! +💲'+ stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(topLine.size == 2 && (this.spinnersList[0].innerHTML == this.spinnersList[1].innerHTML || this.spinnersList[1].innerHTML == this.spinnersList[2].innerHTML)) {
            var stakeReturn = this.getStakeReturn(stake, 1, this.spinnersList[1].innerHTML);
            this.results.innerHTML = '😍 Winner! +💲' + stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(midLine.size == 2 && (this.spinnersList[3].innerHTML == this.spinnersList[4].innerHTML || this.spinnersList[4].innerHTML == this.spinnersList[5].innerHTML)) {
            var stakeReturn = this.getStakeReturn(stake, 1, this.spinnersList[4].innerHTML);
            this.results.innerHTML = '😍 Winner! +💲' + stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(botLine.size == 2 && (this.spinnersList[6].innerHTML == this.spinnersList[7].innerHTML || this.spinnersList[7].innerHTML == this.spinnersList[8].innerHTML)) {
            var stakeReturn = this.getStakeReturn(stake, 1, this.spinnersList[7].innerHTML);
            this.results.innerHTML = '😍 Winner! +💲' + stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else if(diag1Line.size == 1 || diag2Line.size == 1) {
            var stakeReturn = this.getStakeReturn(stake, 1.25, this.spinnersList[4].innerHTML);
            this.results.innerHTML = '😍 Winner! +💲' + stakeReturn;
            this.results.style.color = '#FFF700';
            this.updateCash(stakeReturn);

        } else {
            this.results.innerHTML = '🙁 Spin Again';
            this.results.style.color = '#000000';
        }
    }
}

var spinner = new Spinner();